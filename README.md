Meachine Learning
=================
Este es un proyecto Realizado Por Rodrigo Boet y Leonel Hernandez, con el objetivo de realizar Machine Learning (ml) a modelos de datos, lo cual nos dara predicciones de comportamiento de un patron dado.

El presente proyecto contiene los siguientes directorios:
---------------------------------------------------------
- RegresionLineal:
    Esta comprendido de las funciones o Clases desarrolladas para el calculo de
    predicciones dado los modelos, este directorio actuara como el controlador de los modelos.
- recursos: 
    Esta comprendido por los modelos resultantes de la recoleccione de datos en diferentes momentos 
    o eventos, se guardan o se crean en dicho directorio, para ser consumidos por otros procesos.
- scraping:
    Comprende todos las clases, metodos, script o funciones, que permiten la recoleccion de los datos,
    este directorio actuara como trabajadores para la recoleccion de data y construccion de modelos
- tests:
    Comprende todas todos las clases, metodos, script o funciones, que requieran aplicar pruebas para
    luego ser lanzadas como procesos ya sea como scraping o paquetes que surjan para realizar Machine
    Learning

## Contactos: ##
----------------
    -rudmanmrrod@gmail.com (Rudman)

    -leonelphm@gmail.com (Leonelph)
