# -*- coding: utf-8 -*-
import pyexcel as pycsv

sheet = pycsv.get_sheet(file_name="../recursos/example.csv", delimiter=';')
t = 365

i = 0
sumatoria_registros_evento = 0
sumatoria_registros = 0
sum_eventos = 0
eventos_cuadrado = 0

for row in sheet:
    media_evento = sum(row)/len(row)
    # Sumatorias de los registros del evento por el numero del evento. Paso 1 (Sumatoria de cada X por cada Y)
    sumatoria_registros_evento +=  i * media_evento
    # Sumatoria  de los registro de cada evento Paso 2 Sumatoria de Y
    sumatoria_registros += media_evento
    # Sumatoria del numero de eventos Paso 3 Sumatoria de X
    sum_eventos += i
    # Sumatoria de Cada evento elevado al cuadrado Paso 4 Sumatoria de cada X^2
    eventos_cuadrado +=  i**2
    i += 1

# Sumatoria del conjunto de enventos y se eleva al cuadrado Paso 5
sumatoria_eventos_cuadrado = sum_eventos**2

# [(Numero de eventos por el resultado del paso 1) menos (el resultado del paso 2 por el resultado del paso 3)] entre [(numero de eventos por el resultado de paso 4 )menos (el resultado del paso 5)]
pendiente = ((i*sumatoria_registros_evento)-(sum_eventos*sumatoria_registros))/((i*eventos_cuadrado)-sumatoria_eventos_cuadrado)
# Punto de la interseccion :
media_eventos = sumatoria_registros/i
media_total_evento = sum_eventos/i
punto_interseccion = (media_eventos-(pendiente*media_total_evento))
# Pronostico o prediccion
pronostico = punto_interseccion + (pendiente*t)
valor_medido = pronostico

# Error Absoluto
error_absoluto = abs(valor_medido - sum(sheet.row[t-1])/len(sheet.row[t-1]))

# Error Relativo

error_relativo = error_absoluto/sum(sheet.row[t-1])/len(sheet.row[t-1])

print('La prediccion dado el evento %s, es de %s' % (t, pronostico))
print('El error Absoluto de la medida es: %s ' % error_absoluto)
print('El error Relativo es de: %s' % error_relativo)
