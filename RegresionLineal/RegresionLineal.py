# -*- coding: utf-8 -*-
"""!
Clase RegresionLineal

@author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
@copyright <a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU Public License versión 2 (GPLv2)</a>
@date 12-11-2017
@version 1.0.0
"""


class RegresionLineal:

    def __init__(self, modelo, modelo_verificar, evento):
        """!
            Metodo de inicializacion del objeto

            @params:
            modelo      -Requerido es una hoja de texto plano en formato csv se debe
                               pasar un objeto pyexcel, como por ejemplo:
                               pyexcel.get_sheet(file_name="example.csv", delimiter=';')
            evento       -Requerido numero entero que representa el evento que se desea pronosticar

        """
        self.modelo_verificar = modelo_verificar
        self.modelo = modelo
        self.evento = evento
        self.i = 0
        self.sumatoria_registros_evento = 0
        self.sumatoria_registros = 0
        self.sum_eventos = 0
        self.eventos_cuadrado = 0

    def calPendiente(self):
        """!
            Metodo calPendiente: Calcula la pendiente de la recta

            @return:
            pendiente     -Valor real de la pendiente de la ecuacion de la recta

        """
        for row in self.modelo:
            media_evento = sum(row)/len(row)
            # Sumatorias de los registros del evento por el numero del evento. Paso 1 (Sumatoria de cada X por cada Y)
            self.sumatoria_registros_evento +=  self.i * media_evento
            # Sumatoria  de los registro de cada evento Paso 2 Sumatoria de Y
            self.sumatoria_registros += media_evento
            # Sumatoria del numero de eventos Paso 3 Sumatoria de X
            self.sum_eventos += self.i
            # Sumatoria de Cada evento elevado al cuadrado Paso 4 Sumatoria de cada X^2
            self.eventos_cuadrado +=  self.i**2
            self.i += 1

        # Sumatoria del conjunto de enventos y se eleva al cuadrado Paso 5
        sumatoria_eventos_cuadrado = self.sum_eventos**2

        # [(Numero de eventos por el resultado del paso 1) menos (el resultado del paso 2 por el resultado del paso 3)] entre [(numero de eventos por el resultado de paso 4 )menos (el resultado del paso 5)]
        pendiente = ((self.i*self.sumatoria_registros_evento)-(self.sum_eventos*self.sumatoria_registros))/((self.i*self.eventos_cuadrado)-sumatoria_eventos_cuadrado)
        return pendiente

    def puntoIntersenccion(self):
        """!
            Metodo puntoIntersenccion: Calcula el punto de Interseccion de la recta

            @return:
            pendiente                 -Valor real de la pendiente de la ecuacion de la recta
            punto_interseccion  -Valor real del punto de interseccion de la recta
        """
        # Punto de la interseccion :
        pendiente = self.calPendiente()
        media_eventos = self.sumatoria_registros/self.i
        media_total_evento = self.sum_eventos/self.i
        punto_interseccion = (media_eventos-(pendiente*media_total_evento))
        return (pendiente, punto_interseccion)

    def pronostico(self):
        """!
        Metodo pronostico: Calcula el pronostico

        @return:
        pronostico                -Valor real pronosticado de acuerdo al modelo
        error_absoluto         -Valor real  de la diferencia entre el valor tomado y el valor medido como exacto
        error_relativo            -Porcentaje de erro que se da del cociente de la división entre el error absoluto y el valor exacto.
        """
        # Pronostico o prediccion
        punto_interseccion_pendiente = self.puntoIntersenccion()
        pronostico = punto_interseccion_pendiente[1] + (punto_interseccion_pendiente[0]*self.evento)
        valor_medido = pronostico

        try:
            # Error Absoluto
            error_absoluto = abs(valor_medido - sum(self.modelo_verificar.row[self.evento-1])/len(self.modelo_verificar.row[self.evento-1]))

            # Error Relativo

            error_relativo = error_absoluto/sum(self.modelo_verificar.row[self.evento-1])/len(self.modelo_verificar.row[self.evento-1])*100
        except Exception as e:
            print(e, "No se encuentra el valor exacto para ser comparado")
            error_absoluto = None
            error_relativo = None

        return (pronostico, error_absoluto, error_relativo)
