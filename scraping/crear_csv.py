# -*- coding: utf-8 -*-
import random
import json
import time
import pyexcel as pycsv
import mechanicalsoup


# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

browser = mechanicalsoup.Browser()
list_currency_price = []
list_verificacion = []
list_pruebas = []
longitud = len(list(range(0, 700)))
printProgressBar(0, longitud, prefix = 'Progress:', suffix = 'Complete', length = 50)
for i in range(0, 700):
    try:
            #page = browser.get("https://apiv2.bitcoinaverage.com/convert/global?from=BTC&to=USD&amount=1")
            #data = json.loads(page.content.decode('UTF-8'))
            #print(data['price'])
            page = browser.get("https://coinmarketcap.com/")
            coin_market = page.soup.table.findAll('td')[3].find('a').text[1:]
            #print(page.soup.table.findAll('td')[3].find('a').text[1:])
            page = browser.get("https://api.coindesk.com/site/headerdata.json?currency=BTC")
            other_data = json.loads(page.content.decode('UTF-8'))
            #print(other_data["bpi"]["USD"]["rate_float"])
            page = browser.get("https://blockchain.info/es/ticker")
            blockchain_data = json.loads(page.content.decode('UTF-8'))
            #print(blockchain_data['USD']['last'])
            page = browser.get("https://api.coinbase.com/v2/exchange-rates?currency=BTC")
            coinbase_data = json.loads(page.content.decode('UTF-8'))
            #print(coinbase_data['data']['rates']['USD'])
            page = browser.get("https://bitpay.com/api/rates/usd")
            bitpay_data = json.loads(page.content.decode('UTF-8'))
            #print(bitpay_data['rate'])
            page = browser.get("https://api.bitfinex.com/v1/trades/btcusd")
            bitfinex_data = json.loads(page.content.decode('UTF-8'))
            #print(bitfinex_data[0]['price'])
            page = browser.get("https://www.bitstamp.net/api/v2/ticker/btcusd/")
            bitstamp_data = json.loads(page.content.decode('UTF-8'))
            #print(bitstamp_data['last'])
            list_currency_price += [coin_market, other_data["bpi"]["USD"]["rate_float"],
                                                   blockchain_data['USD']['last'], coinbase_data['data']['rates']['USD'],
                                                   bitpay_data['rate'], bitfinex_data[0]['price'], bitstamp_data['last']], 
    except Exception as e:
        print(e)
        list_currency_price +=[0, 0, 0, 0, 0, 0, 0],
    if i <=340:
        list_pruebas = list_currency_price
    else:
        list_verificacion = list_currency_price
    time.sleep(5)
    printProgressBar(i + 1, longitud, prefix = 'Progress:', suffix = 'Complete', length = 50)


pycsv.save_as(array=list_verificacion, dest_file_name="../recursos/modelo_verificacion.csv", dest_delimiter=';')
pycsv.save_as(array=list_pruebas, dest_file_name="../recursos/modelo_prueba.csv", dest_delimiter=';')
