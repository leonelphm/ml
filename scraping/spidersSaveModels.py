# -*- coding: utf-8 -*-
import random
import json
import time
import datetime
import os
import pyexcel as pycsv
import mechanicalsoup

browser = mechanicalsoup.Browser()
list_currency_price = []
dia_anho = datetime.date.today().strftime("%j")
fecha = time.strftime("%Y")
ruta_csv = 'modelo_medicion_%s.csv' % (fecha)
ruta = os.path.join('recursos', ruta_csv)
open(ruta, 'a').close()
time.sleep(5)
try:
        page = browser.get("https://apiv2.bitcoinaverage.com/convert/global?from=BTC&to=USD&amount=1")
        data = json.loads(page.content.decode('UTF-8'))
        bitcoinaverage = data['price']
        page = browser.get("https://coinmarketcap.com/")
        coin_market = page.soup.table.findAll('td')[3].find('a').text[1:]
        page = browser.get("https://api.coindesk.com/site/headerdata.json?currency=BTC")
        other_data = json.loads(page.content.decode('UTF-8'))
        coindesk = other_data["bpi"]["USD"]["rate_float"]
        page = browser.get("https://blockchain.info/es/ticker")
        blockchain_data = json.loads(page.content.decode('UTF-8'))
        blockchain = blockchain_data['USD']['last']
        page = browser.get("https://api.coinbase.com/v2/exchange-rates?currency=BTC")
        coinbase_data = json.loads(page.content.decode('UTF-8'))
        coinbase = coinbase_data['data']['rates']['USD']
        page = browser.get("https://bitpay.com/api/rates/usd")
        bitpay_data = json.loads(page.content.decode('UTF-8'))
        bitpay = bitpay_data['rate']
        page = browser.get("https://api.bitfinex.com/v1/trades/btcusd")
        bitfinex_data = json.loads(page.content.decode('UTF-8'))
        bitfinex = bitfinex_data[0]['price']
        page = browser.get("https://www.bitstamp.net/api/v2/ticker/btcusd/")
        bitstamp_data = json.loads(page.content.decode('UTF-8'))
        bitstamp = bitstamp_data['last']
        list_currency_price = [dia_anho, bitcoinaverage, coin_market,
                                    coindesk, blockchain, coinbase, bitpay,
                                    bitfinex, bitstamp]

except Exception as e:
    print(e)
    list_currency_price = [dia_anho, 0, 0, 0, 0, 0, 0, 0, 0]

if (not os.path.isfile(ruta)):
    pycsv.save_as(array=list_currency_price, dest_file_name=ruta, dest_delimiter=',')

else:
    list_currency_update = pycsv.get_sheet(file_name=ruta, delimiter=',')
    list_currency_update.row += list_currency_price
    list_currency_update.save_as(ruta)
